FROM alpine:3.17

COPY --from=justmiles/markdown2confluence /markdown2confluence /bin/markdown2confluence

ENTRYPOINT ["/bin/sh"]
